Initial commit

À vos marques

prêts

Et si l'avenir de la révolte passait par

des écritures non littéraires

Je pense notamment à l'écriture

informatique et sa tradition du "hack"

La fabrique d'errance littéraire

Innommable s'est ainsi lancée depuis

quelques années dans une patrique

éditoriale qui passe par le protocole du

vide et la plateforme Gitlab

Il s'agit de créer des "antimatières"

dont les contenus mais aussi les gabarits

sont déposés en ligne sur des repos du

vide, entièrement réappropriables

J'en ai parlé il y a quelques semaines

lors d'un colloque depuis mon salon

Je te dépose ici mes notes

Un sujet tout juste défriché, des

remarques sans doute très préliminaire

et un registre oral

reste à transformer l'essai

Cela fait un moment que l'on discute de

la question toi et moi, et avec d'autres

bien-sûr

Nous avons édité et écrit sur git

depuis presque 5 ans

Et voilà qu'un éditeur, une fabrique

ou un collectif, ce n'est pas clair,

s'est pleinement emparé du protocole et

de la plateforme gitlab pour éditer et

publier ses ouvrages

Mais avant de rentrer dans ton texte,

nous avions besoin de notre propre

protocole d'écriture et de pensée

C'est une expérimentation gratuite,

nous ne savons pas ce qui en ressortira,

une belle aberration au mieux, quelques

idées nouvelles au pire, et certainement

encore de la pratique, de celle qui nous

fait penser ensemble

Ce qui compte ici, c'est l'ouverture,

celle du processus d'écriture, et celle

du protocole même

En voici une première version qu'on

fera évoluer

Après tout, les règles du jeu sont

faites pour s'amuser

Tu évoques le collectif comme l'une des

intentions d'

Innommable, et je trouve que l'on

pourrait développer cet aspect

Il fera sans doute écho à la notion

d'autorité, peut-être aussi à la

notion d'écologie, tant l'environnement

d'écriture et d'errance est devenu

écologique

Le choix d'une plateforme comme Gitlab

est assez emblématique d'une vision

environnementale de l'écriture et de

l'errance

décidémment je n'arrive pas à les

séparer ce deux-là

Mais alors, collectif ou écologie

En fait les deux se rejoignent assez

vite puisque le choix du milieu est

bien un choix politique, considérant que

l'écriture et ses modalités, notamment

collectives, peuvent relever d'une

écologie politique

J'ai un peu parlé de ça dans la

thèse, faisant le lien entre les

rhétoriques du nous, du collectif, du

faire

fabriquer, éditer

et d'une vision écologique

Une question qu'on pourrait se poser

c'est comment le collectif se constitue

chez

Innommable, comment il s'est réalisé

faire collectif

notamment pour la ZAP Rimbaud

Et au-delà de la ZAP,

Innommable continue de publier des

auteur·e·s

Certes les éditeurs et éditrices

s'effacent derrière un même compte

gitlab

comment intérpêter ça

mais dans un nous finalement très

peu inclusif

Qu'en penses-tu

Mes yeux ont fourché, je t'ai mal lu

Tu parlais de "diffusion" et j'y ai lu

"fusion"

Mais cela fonctionne finalement bien

pour parler d'écologie

Tu as raison, le mot écologie est sans

doute plus juste que collectif

Innommable redéfinit-il

elle

quel est son genre

la fonction éditoriale, ou n'est-il

elle pas plutôt en train de virtualiser

le concept, c'est-à-dire de revenir à

ce qui en faisait l'essence initiale,

avant qu'un modèle éditoriale moderne

ne distribue les rôles et ne fige

certains concepts, dont celui d'auteur

Tu parles d'un nous qui n'est pas

inclusif, certes, et en même temps

quelle envie de travailler ensemble

La tension entre la sociabilité propre

à l'écrivain

mais on en dira autant du chercheur

et la nécessité d'affirmer sa

singularité est propre à tout mouvement

collectif, ce n'est pas nouveau

Je crois qu'en permettant la

réappropriation et en encourageant la

pratique du hack, issue des écritures

informatiques

une autre branche à créer

Innommable offre une solution pour

négocier cette tension

C'est là sans doute où le terme

d'écologie, et plus encore de "milieu",

me semblent pertinents, car ils

sous-entendent des processus à l'oeuvre,

dont ces négociations toujours en cours

Virtualiser la fonction éditoriale

tout un programme

C'est une image que tu m'as soufflée

lorsque je rédigeais ma thèse

Considérer les formes comme toujours en

devenir, toujours "en puissance"

Les textes, bien entendu, mais aussi les

protocoles

Pourquoi ne pas appréhender les

protocoles comme des objets à

travailler, à façonner, si possible

collectivement

Et de fil en aiguille, au-delà des

textes et des protocoles qui les

fabriquent, virtualiser aussi la fonction

éditoriale, c'est-à-dire ne plus la

figer, mais l'émanciper d'une

définition et la couler, la fondre dans

le flux des écritures, en reconnaissant

le caractère processuel

et environnemental

de l'écriture, de la légitimité, et

on le voit, de la légitimation

Ah

mais tu me tends une autre perche, la

négociation

Négociations, conversations, ce par

quoi adviennent ces multiples niveaux

d'écritures, les textes, les protocoles,

les codes, les éditorialisations

Ce par quoi adviennent aussi les

collectifs, liés de consensus et

tiraillés de controverses, mais bien

noués par un processus continu de

conversations

Écrire, pour moi, c'est actualiser une

pensée

Cette inscription a le mérite de

clarifier de ce que je pense, mais elle a

tendance à figer par la même occasion

cette pensée

Tout ce dont nous parlons ici, cette

émulation collective, disparaîtra de la

version finale

Le texte publiégomme l'élaboration de

la pensée et tout ce/celle/ceux qui ont

participé à cette élaboration

C'est cela que du vide nous permet sans

doute de rétablir

Par des états du texte, par une

visualisation du workflow, quelques

étapes du flux de pensée parvient à

subsister

Je crois à présent qu'il nous faut

ouvrir une nouvelle branche

conversationnelle

Je te propose de penser au protocole

Merge branch 'collectif' into 'master'

Attends attends, avant de parler

protocole, je te propose de strucuturer

un peu

Après tout, on a suffisament répété

qu'écrire numérique consistait à

structurer

C'est un texte encore court, alors ne

diésons pas, niveau 1 je dirais dans un

premier temps, ce n'est pas encore une

titraille conceptuelle, plus quelques

repères de lecture

Mais on y décèle déjà un parcours et

un propos

Goody n'avait décidément pas tort

Écrire, c'est structurer, c'est

conceptualiser

J'accepte donc cette domestication de ma

pensée sauvage qui me permet de

problématiser plus clairement ce que je

veux dire

On ne défend pas le WYSIWYM pour rien

L'utilisation de Gitlab comme outil

d'écriture et d'errance me semble

important dans le cadre d'une réflexion

sur le vaste mouvement de

plateformisation qui touche les

écritures numériques littéraires: on a

beaucoup commenté, parfois loué,

parfois condamné, les plateformes de

type Wattpad, ou encore les CMS comme

Spip, Wordpress, très utilisés par les

communautés d'écrivains

avec d'ailleurs une certaine hiérarchie

plus ou moins explicite se dessinant

entre les écrivains Pro qui utilisent

plutôt SPIP, par exemple, et les

écrivains amateurs qui se tourneront

vers Wattpad

Gitlab, parce qu'il repose d'abord sur

un protocole, et parce qu'il s'adresse à

une autre communauté, celle des

développeurs, propose un modèle

alternatif d'écriture

Mais du coup, un autre modèle

conceptuel d'écriture: j'ai parlé du

hack, de l'autorité partagée

Je ne suis pas sûre cependant d'aller

assez loin dans ma description des

conséquences épistémiques d'un tel

protocole

L'errance littéraire a ses protocoles,

souvent implicites

Je crois qu'aujourd'hui ces protocoles

ne conviennent d'ailleurs plus à nombre

d'écrivains qui se sont rués vers des

sites persos, convaincus de se délivrer

des éditeurs : une désintermédiation

illusoire, puisque ce sont les

plateformes qui ont repris cette place en

partie

Je te lance la balle, sans doute de

travers

C'est que je ne sais pas trop où je vais

Entorse au protocole du vide

J'ai écrit

involontairement

sur la mauvaise branche

Tant pis, il faudra corriger

De quoi noter combien la littératie est

importante, et constitue un petit défi

dans ce que nous tentons de mettre en

oeuvre

J'ouvre donc une nouvelle branche, forge

Elle me ramène à l'autorité

Ce qui me plait dans notre petite

expérience, c'est qu'elle inscrit ce

qui, trop souvent, demeure caché: les

conversations, les brainstorms, grâce

auxquels nous avançons

En utilisant git, nous nous mettons

aussi à nu: nous montrons les

tatônnements, les ratés

Mais n'est-ce pas dans ces imperfections

que nous apprenons le plus

Il y a un enjeu majeur, au sein de

l'Université, à reconnaître toutes les

contributions

Git permet sans doute cette

reconnaissance

Mais l'autorité qui se dessine n'en est

pas moins problématique

Finalement, le modèle de l'errance

traditionnelle à la papa a le mérite

d'éviter de nous poser des questions

Aussi je te relance ici sur le thème de

la négociation: le protocole fonctionne

s'il sait s'adapter

Plus que moi, tu as participé dans la

communauté des communs à ces

négociations

tu disais plus tôt que le nous

Innommable n'était pas inclusif

Et en relisant le manifeste je me risque

à me demander : comme dans toutes les

avant-gardes,

Innommable risquerait-il l'autoritarisme

Je tente de ne pas répondre de travers,

mais je crains de répondre à côté

En relisant la partie que tu as

travaillé, je me demandais si le trait

n'était pas un peu forcé

Disons qu'effectivement la gittérature,

en investissant une plateforme et un

protocole initialement pensés pour

l'écriture de code, devrait s'emparer

tout autant du code que du texte

Mais ce n'est pas exactement cela qu'il

se passe, en tout cas pour le cas

Innommable

En effet, d'une part l'usage de git dans

le domaine littéraire ou éditorial est

avant tout

c'est-à-dire avant la création de

nouvelles formes

un formidable outil d'errance au sens

traditionnel, permettant d'investir des

formats et des standards plus vertueux,

ou encore de tracer l'historique des

contributions et la genèse d'une texte

D'autre part, il n'est pas certain que

les auteur·e·s d'

Innommable aient réellement mis la main

à la git-pâte

Considérons qu'il y en a quelqu'un, et

envisageons un entretien

Enfin pour me contredire, il est vrai ue

les éditeurs et·ou éditrices d'

Innommable sont allés plus loin qu'une

simple errance raisonnée, et savent

aussi bien jouer de la plume que du code

pour la faire danser

C'est sans doute l'un des plus grands

risques des études littéraires que de

monumentaliser et essentialiser son objet

Je ne suis pas objective lorsque je

parle d'

Innommable, parce que leur Idée

et idéal

de révolte correspond à mes propres

utopies

Mais qui demeurent, sans doute, à

l'état d'utopie puisque comme tu le dis

très bien, peu de projets sont parvenus

à réaliser le principe d'errance et

d'écriture partagée

La ZAP Rimbaud fait figure d'exception,

mais c'est une preuve de concept qui doit

nous encourager à poursuivre notre rêve

un peu fou

La dimension politique, plus que

politique, de la gittérature se dessine

mieux dans mon esprit et dans le texte,

je crois, à présent

Si comme le dit A

Gefen la révolte est une idée, ayons

le courage de nos idées, alors

Nous en avons parlé, au-delà de

l'autoritarisme, c'est davantage un

certain idéalisme qui les mobilise, et

qui les place nécessairement en

résistance face à des pratiques plus

traditionnelles

Il y a chez

Innommable une vision, que l'on partage

également nous-même en tant qu'éditeur

et éditrice, et en tant que chercheur·e

ayant saisi l'enjeu de la matérialité

des écritures

Forcément, ça bouscule un peu

Le thème de la négociation élargit

encore le paysage, à tel point que je me

demande si nous devrions pas écrire deux

articles

Ou tout un livre peut-être

En lien avec ce concept, je note ici

quelques pistes en vrac à développer

plus tard : la bienveillance du

dispositif, ou ici du protocole, qui tout

à la fois contraint et accueille les

pratiques dans leur diversité

les possibles qu'ouvrent des plateformes

telles que gitlab pour tracer/formaliser

les échanges informels qui participent

de la fabrique d'un texte, il y a là à

nouveau un cadre vertueux à la

négociation, tant pour la reconnaissance

des contributions que pour la dynamique

constructive qu'elle instaure

Je ne parlerais pas de commun encore

ici, mais plutôt de confiance

Nous mettons tellement de confiance dans

les écrits, c'est étonnant

cela se vérifie encore ici

Git, en tant que technologie du

registre, c'est-à-dire de

l'enregistrement

inscription

des différentes actions, git donc

produit avant tout de la confiance entre

les différents acteurs

Ah mais

je retombe sur mes pattes, ou plutôt

sur mon dernier commit c7224bc1

En fait tout ça se rejoint

Il me semblait bien que protocole et

forge pouvaient fusionner

Je vais tenter quelque chose dans ce sens

Nous avons lancé plusieurs lignes à la

fois, et nous en tirons maintenant les

fruits

ou les poissons

En tout cas les idées fusent de

partout, le fonctionnement de cette

conversation asynchrone est une belle

preuve de concept à la fois de la

gittérature, et à la fois du concept de

conversation

Merge branch 'forge' into 'protocole'

Protocoles, forges, gitlab

notre conversation est pour le moment

centrée essentiellement sur le git de la

gittérature

Pour répondre à nos questions, j'ai

besoin de revenir à mes premières amours

Je propose de mettre à jour la branche

maîtresse de notre travail, qui commence

décidément à prendre chair, pour

proposer d'abord une réflexion sur le

hack et l'écriture sans écriture, puis

plonger à coeur perdu dans la ZAP Rimbaud

La gittérature est idéaliste, mais il

s'agit aussi, je crois, d'un modèle tout

à fait applicable

Tu as raison, nous en donnons la preuve,

et il y a d'ailleurs je crois chez nous

la volonté de bien ecrire nos commits

Une écriture à contrainte, en fait,

qui pour cette même raison n'est pas

dénuée de littérarité

Merge branch 'protocole' into 'master'

L'un des défis, pour penser la

gittérature, consiste je crois à

articuler l'écriture informatique et

l'écriture littéraire

Le paradigme du hack me semble une piste

intéressante, je vais la brancher

Il est temps aussi, je crois, de revenir

sur l'expérience ZAP

Place à la poésie

Depuis ma lecture des travaux de

Gabriella Coleman, je suis fascinée par

les pratiques de hacking

Une fascination qui s'explique sans

doute par la proximité de l'esprit du

hack avec tout ce qui m'a toujours plu en

révolte : le détournement, la parodie,

la satire, l'expérimentation des formes

Mais qui s'explique aussi par la

dimension collective et ludique de la

pratique : comme Biella a pu le

démontrer dans ses travaux sur

Anonymous, il n'est pas nécessaire de

disposer de grandes compétences

techniques pour produire un bon hack, il

suffit simplement d'être nombreux

La force du groupe, je serais tentée de

dire une intelligence collective si

celle-ci n'était pas aussi

imprévisible, se trollant elle-même,

permet de faire trembler les plus grande

banques : juste une simple saturation du

serveur

Je relie donc le hacking au paradigme

contemporain à travers Goldmisth, une

écriture sans écriture

Je lâche le concept

d'éditorialisation, sans doute un peu

rapidement, je ne suis pas sûre de moi

Je ne suis pas certaine de bien formuler

ce qui relie toutes ces pratiques: une

nouvelle forme d'autorité

Une reconnaissance des états du textes

donc de la pensée

Je suis dans une impasse

D'une part, je n'ai pas grand chose à

ajouter à l'écriture de cette section,

d'autre part, j'ai trop de choses à dire

en réponse à ton dernier commit

J'ai presque envie de forker encore,

peut-être pour emmener le texte dans une

autre direction

Voyons un peu

Autres pratiques du hack sur lesquels

j'ai un peu travaillé, ce sont le mashup

et le remix, avec leur esthétique et

leur culture bien à eux

On peut parler dans une certaine mesure

d'écriture sans écriture, les artistes

qui se prêtent au jeu vous diront bien

à quel point l'errance est une écriture

Tout deux, le remix pour la musique et

le mashup pour la vidéo

pour caricaturer

se positionnent aussi politiquement,

comme une contre-culture rejettant

catégoriquement certains aspects de la

propriété intellectuelle

Il suffit de penser à l'excellent RiP:

A Remix Manifesto de Brett Gaylor

L'avènement des Creative Commons et du

mouvement Culture Libre n'y sont pas

étrangers

Tu cherchais un lien entre toutes ces

pratiques, je crois sincèrement que le

principe du copyleft, originaire de la

communauté informatique, est sans doute

ce qui a posé les bases communes pour

une culture du partage et de la

transparence, dans laquelle la

gittérature prend racine

Mais tu cites déjà Coleman et cet

ethos libertaire, donc tout est dit

Concernant l'éditorialisation, il y a

une véritable question qui n'est encore

que très peu abordée dans le texte

Voici mon analyse: Je dirais que

l'éditorialisation dans la gittérature

est celle de la plateforme, dans la

manière dont les fragments de textes et

les traces d'activités sont agencées

pour susciter le partage, la

collaboration et l'élan collectif vers

une création

Mais le texte *gittéraire*,

c'est-à-dire fondamentalement un texte

travaillé collectivement dans un format

plein texte, est davantage régit par le

registre et le principe de la forge, la

fusion des modifications

L'éditorialisation intervient dans les

couches applicatives de la plateforme

gitlab, là où l'on manipule les

manipulations du texte

Je vois donc l'éditorialisation

performer davantage autour du texte que

performer le texte lui-même

Le texte n'est d'ailleurs peut-être

qu'un prétexte pour autre chose: le

collectif, la circulation, l'interstice

où se niche l'interprétation et la

création

Je me pose soudain une question

Mais peut-être Coleman y aura répondu

Penses-tu que la tendance à

l'optimisation du code qui est la

motivation première des partages de code

aient une incidence sur la création

gittéraire

Que devient l'esthétique minimaliste du

code qui cherche constamment à

améliorer le temps de calcul des

processeurs ou les accès aux disques

Y a t il un détournement de cette

tendance

Est ce que la ZAP Rimbaud nous éclaire

là-dessus

Nous voici enfin en plein dans la ZAP

Décrire l'événement est moins simple

que je ne le pensais

Je souhaite aussi rentrer dans le texte,

il faudrait citer des extraits

Comment produire une belle ekphrasis du

dispositif final de l'antimatière, dans

une publication destinée à être

imprimée

comprendre : sans image, qui plus est

sans dynamisme évidemment

Plusieurs pistes me viennent pour

écrire cette partie, sans doute la moins

fournie et écrite que les autres

Il faut dire que dans le texte original,

une communication orale, je me contentais

de montrer et de commenter des images

Cette fois-ci il faudra être plus

rigoureux

Me vient d'abord la question de la

communauté, de la conversation

nécessaire et de la re-plateformisation

par Gitter

C'est un premier axe

Me vient ensuite l'idée d'expliciter la

poétique du hack via

l'écriture-palimpseste - tu voudras sans

doute, avec raison, la relier à la

stratification des textes

Peut-on dire, peut-être, que

l'antimatière ZAP est une métaphore

visuelle du protocole et, donc, de la

gittérature

Je vais peut-être trop loin

Fatigue

Un push et au lit

Ce que tu dis à propos de

l'éditorialisation me semble capital, je

crois qu'il faudra le répéter en

conclusion - je laisse pour l'instant

cela de côté, et je réagis plutôt à

ta dernière remarque

En effet, le principe premier de du vide

est d'abord l'optimisation, ou

réusinage, ou encore refactorisation

des termes que je découvre en faisant

mes recherches

Pourtant, dans les faits, ce n'est pas

toujours le cas, du moins je crois

Je ne suis pas développeuse, mais ce

que j'ai appris en cotoyant les dév dans

le cadre de mon activité

professionnelle, c'est qu'il existe

plusieurs façons d'écrire du code

Plusieurs langues, plusieurs registre

Je suis pas sûre qu'il y ait consensus

sur la *meilleure* façon de coder, bien

qu'il y existe de recommandations et des

bonnes pratiques

Je dirais que, de manière pragmatique,

chaque projet doit pouvoir trouver son

langage informatique

Encore une fois, tout nous ramène à la

ZAP et il faut s'y plonger sérieusement

La ZAP n'a pas vocation à optimiser le

poème rimbaldien

Elle l'augmente de manière conséquente

Elle y ajoute des strates de texte, de

sens, d'impression, d'hommage

Jusqu'où comparer l'écriture

informatique et l'écriture poétique

Dans les critical codes studies, j'ai

l'impression que la beauté du code

importe peu: les effets produits par le

code compte davantage

Mais je suis peut-être trop radicale

Qu'en est-il en gittérature

Ce n'est plus ici le lieu pour y

répondre

Débranchons pour de bon

Merge branch 'hack' into 'master'

Je commence à *objectiver* ou

*légitimer* notre réflexion en la

bariolant de références

J'exagère : la bibliographie qui se

dessine est un peu ma bibliothèque

théorique idéale

Le plaisir d'écrire une conclusion

*ouverte* en sachant que, pour une fois,

elle l'est vraiment

Ce texte est une ébauche, vers une

réflexion que j'espère plus large, à

deux ou à plus encore

Conclure est moins douloureux, moins

compliqué, lorsque l'on sait qu'on a

tout le temps d,éditorialiser un texte,

et que ce n'est là qu'une étape de

notre pensée

Je lance une proposition un peu

rhétorique : les pratiques non

littéraires sont-elles l'avenir de la

révolte

La révolte est-elle l'avenir de la

littératie numérique

en restant bien consciente de

l'artificilité d'une telle démarche

Mais puisqu'il s'agit ici de faire,

aussi, une preuve de concept, pourquoi

pas

Voilà, j'ai tenté quelque chose, mon

"ekphrasis" s'est focalisée sur le

protocole -- notre sujet après tout,

plutôt que sur la communauté et les

espaces conversationnels

J'avais préparé quelque chose à ce

sujet en reprenant tes points, mais

finalement, je pense que c'est à jeter

vu le nombre de caractères qu'il nous

reste

à supprimer

Sur le protocole, je pense que tout y est

Tu as raison: pas aisé de décrire en

quelques paragraphes un tel processus

collectif s'appuyant sur une mécanique

aussi complexe et une série d'interfaces

abscontes pour le néophyte

Nous verrons si nous sommes bien compris

À vrai dire, même avec des images, il

y aurait un petit défi éditorial à

illustrer le même propos sans perdre

le·la lecteur·lectrice

Je te laisse nettoyer les paragraphes

suggérés à la suppression, et mettre

une touche finale à ma prose

Qui nous lira

L'article que nous écrivons me semble

parfois bien technique

Je complète ton ekphrasis en jouant à

la poéticienne

L'occasion de me replonger dans le texte

original de Rimbaud, puis le texte de la

ZAP, j'y ai passé l'après-midi, que du

bon temps

Qui lira la ZAP

A-t-elle du sens en dehors de

l'événement

C'est le défi de la révolte qui se

frotte au paradigme contemporain

L'idée de révolte, ou plutôt une

certaine idée de révolte, compte

davantage que le résultat

C'est ce que je peux reprocher à la

révolte électronique, pendant de

critical code studies parfois imbitable

Et pourtant, la ZAP se lit plutôt bien

Je tisserais peut-être ici un

parallèle avec une autre tradition

littéraire qui me semble avoir poussé

la contrainte ludique à un incroyable

niveau de perfection : l'OuLiPo

Lire Perec satisfait certainement notre

goût de l'énigme, du jeu de mot

Mais comment échapper au *punctun* de

ses textes

Comment ne pas se glacer à la fin de

*W*

Comment ne pas pleurer en lisant les

dernières pages de *La vie mode d'emploi*

Et finalement, je m'interroge sur le

sens de nos écritures *savantes*:

pourquoi, pour qui écrit-on

Qui nous déliera

Quel plaisir et quelle frustration aussi

Mais les deux vont ensemble bien entendu

Ce texte est une collection de tiroirs

que nous n'avons fait qu'entrouvrir

Je te trouve courageuse d'en dessiner

deux nouveaux, mais je joue le jeu avec

plaisir

Je vois surtout une feuille de route

pour penser plus loin la gittérature et

avec elle l'écriture numérique

Ce qui me plait, c'est à quel point la

gittérature matérialise en miniature ce

milieu d'écriture dont j'ai parlé dans

la thèse

Il y a soudain là visibles et évidents

tous les ingrédients d'une écriture

dispositive

Zapper génère toujours des conflits

Merge branch 'ouvertures' into 'master'

Sans état d'âme je coupe

Optimisation du texte, comme on

tenterait de clarifier le code

Et finalement, ce n'est pas si mal

Tentative d'objectivation

Au risque d'institutionnaliser notre

gittératurem un brin dommage

Des inspirations éparses, littéraires,

théoriques, médiatiques, philosophiques

J'ai honte

Les éditeurs nous ont prévenu qu'un

article était retiré, nous aurions donc

toute la place nécessaire pour finaliser

l'article

Motif: le texte d'Alexandra Saemmer

aurait été "publié sur le web"

Retiré

Ce n'est plus une serpe, c'est une

guillotine

Qu'un tel motif serve à libérer notre

texte si engagé sur l'ouverture, je

trouve ça douloureux

Honte et colère

Voilà ce qu'on va faire : considérons

ce texte comme un troll, mieux, un cheval

de troll en mission aux confins de

l'enfer : l'errance scientifique privée,

classique, et réfractaire à toute

ouverture

Je n'ai aucune illusion sur l'impact

ZERO de notre *papier*, si ce n'est une

légère pesanteur sur quelques étagères

Mais le troll sera en place, la honte un

jour changera de camps, et puisque le

texte et ce métatexte circuleront

librement et joyeusement, la preuve sera

faite que les barrières propriétaires,

toutes garnie·r·s soient-elles,

relèvent désormais du soubresaut

Futiles et grotesques

Merge branch 'serpe' into 'master'

Cette première étape va bientôt se

terminer

Ici

je-renomme-un-fichier-selon-les-bonnes-pra

tiques, mais c'est surtout un prétexte

pour augmenter le métatexte

Je suis assez fasciné par notre petite

expérience

Le protocole imaginé a donné lieu à

une véritable conversation, a su nous

imposer un rythme d'écriture efficace

Ce métatexte a été un espace

d'écriture libre et d'ouverture

extrêmement précieux

Sans parler d'un terrain d'étude à

venir, autant pour son analyse que pour

son amélioration

Pure recherche-création, j'ai hâte de

disséminer la méthode vers une

recherche-action

Voici un aperçu

Plaisir de la chaîne

Qu'en penses-tu

Quelques coquillages sur le rivage

Pour la choucroute garnier il s'agit

maintenant de produire un docx

Je propose de brancher cela pour ne pas

polluer notre arbre

Il sera toujours temps d'élaguer

2020, annus horribilis ou

mirabilis

L'e dans l'eau fait ploufe

Louise nous manque, sa pensée nous

éclaire toujours

Pluriels plus liés & poêle à thèse

Quid de Guido

hacktion poétique

