> La vie des formes dépend non seulement du lieu où elles existent réellement mais aussi de celui où elles sont vues et se recréent. (Baltrusaitis, *Aberrations*, Les perspectives dépravées t.I, 1995, p.11)

Nous partagerons ici avec Jurgis Baltrusaitis une passion pour les "perspectives dépravées": formes aberrantes, illusions d'optiques, anamorphoses et autres déformations (in)volontaires dont nous souhaitons souligner la puissance heuristique. Regarder de travers, en travers, pour voir autrement (mieux ?). En arts visuels, l'aberration peut renvoyer à l'anamorphose, une image "contre" (à côté et en décalage) une autre image. Un sens caché, mais sans doute le plus important à saisir. Dans *La perspective comme forme symbolique*, Panofsky a démontré combien la perspective défendue par les peintres de la Renaissance impliquait une structure conceptuelle avec des valeurs propres: c'est notamment cette structure que l'anamorphose viendra briser, en suggérant une multiplicité du réel et surtout des expériences du réel. L'aberration, promue au rang de modèle d'écriture, doit nous permettre de faire émerger des perspectives nouvelles, anamorphiques (en jouant comme on va le voir "contre le texte", sous la forme de commits). Notre "perspective" à nous, celle que nous souhaitons déconstruire ou détourner, est celle des protocoles traditionnels de l'écriture scientifique et de son système de légitimation -- qui nous semblent figer la pensée dans un modèle épistémique empêchant la plupart du temps la création de nouvelles formes de savoirs. Nous souhaitons donc anamorphoser l'écriture de l'article scientifique. Comment procéder ? 

**Parce que nous ne pensons jamais seul.e.s**, nous allons mener une conversation, ou plutôt des conversations, chacune ouverte sur une branche _git_ dédiée. Chaque branche abordera un concept, une thématique du texte. Ces conversations s'incarneront au travers des _commits_ de chacun des participants. Ensemble, ces commits tendront à faire sens au niveau de leur branche et s'agrégeront en une conversation plus ou moins maîtrisée. 

**Parce que la pensée naît aussi de la forme**, chaque branche sera l'occasion d'expérimenter une contrainte formelle pour l'écriture des _commits_. Des contraintes qui peuvent reprendre les grands classiques oulipiens (lipogramme, etc), mais aussi des contraintes à inventer, issues des écritures numériques (comment, par exemple, adapter la contrainte des poèmes de métro à la temporalité du web ?). 

**Parce que la pensée n'est pas figée, mais sans cesse en mouvement**, nous souhaitons visibiliser l'ensemble de ce dispositif d'écriture: les états successifs du texte, les échanges qui en discutent, et ce protocole. L'écriture est un processus que nous voulons ouvert.

Mais de quoi allons-nous converser ? 

Les conversations vont venir discuter et travailler ce texte [gitterature.md](gitterature.md) écrit par Servanne Monjour. Les fragments de conversation se feront soit l'écho soit la source des modifications (_diff_) suggérées ou implémentées dans le texte initial.

Ce protocole ne définit pas comment le texte est travaillé, les auteur·e·s restent libres. Mais nous adoptons deux contraintes _structurelles_ : 1) nous rassemblons les modifications (et leurs conversations) par concept abordés, par branche donc, 2) nous expérimentons la syntaxe [critical markup](http://criticmarkup.com/), par pure abérration: 

- `{++ ++}` pour ajouter du texte
- `{-- --}`pour supprimer du texte 
- `{~~ ~> ~~}` pour substituer du texte 
- `{>> <<}` pour commenter
- `{== ==}{>> <<}`pour surligner et commenter

Un "nouveau jeu intéressant" aurait dit Perec.


