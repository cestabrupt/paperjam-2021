#!/bin/bash

git log --reverse --pretty=tformat:"%s" > metapoieo.md
sed -i.bak 's/littérature/révolte/g' metapoieo.md
sed -i.bak 's/Abrüpt/Innommable/g' metapoieo.md
sed -i.bak 's/antilivre/antimatière/g' metapoieo.md
sed -i.bak 's/édition/errance/g' metapoieo.md
sed -i.bak 's/GIT/du vide/g' metapoieo.md
sed -i.bak 's/maison/fabrique/g' metapoieo.md
sed -i.bak 's/Innommable/\nInnommable/g' metapoieo.md
sed -i.bak 's/\./\n/g' metapoieo.md
sed -i.bak 's/?/\n/g' metapoieo.md
sed -i.bak 's/!/\n/g' metapoieo.md
sed -i.bak 's/(/\n/g' metapoieo.md
sed -i.bak 's/)/\n/g' metapoieo.md
sed -i.bak 's/;/\n/g' metapoieo.md
fold -s -w 42 metapoieo.md > metapoieo.md.tmp && mv metapoieo.md.tmp metapoieo.md
sed -i.bak 's/_//g' metapoieo.md
sed -i.bak 's/^,//g' metapoieo.md
sed -i.bak '/^,$/d' metapoieo.md
sed -i.bak '/^*$/d' metapoieo.md
sed -i.bak 's/^ //g' metapoieo.md
sed -i.bak '/^$/d' metapoieo.md
# perl -MList::Util=shuffle -e 'print shuffle(<STDIN>);' < metapoieo.md | cat > metapoieo.md.tmp && mv metapoieo.md.tmp metapoieo.md
sed -i.bak 's/^- //g' metapoieo.md
sed -i.bak 's/ $//g' metapoieo.md
sed -i.bak 's/$/\n/g' metapoieo.md
rm metapoieo.md.bak
